# Modular Talk

## Introduction

Modular synthesizers are devices that can be used to generate audio siganls (or video, or perhaps infact other media, I'd have to check).
They differ from regular synthesizers in that the underlying components that comprise the inner workings of the device, that are usually hidden away, are exposed to the user.
So while a regular synthesizer will have internal connections  between "modules" be "hard-soldered", a modular synthesizer with allow for connections between "modules" to be modified.
These connections are usually made availble via patch sockets, which can then be connected together using cables of some form (1/4 inch jack, minijack, 4mm banana).
Though in some cases this can be a matrix of switches instead.
The first modular synths were created in the early 60s. Immediately, there were 2 schools that emerged, they were refered to as the east-coast ana west-coast schools. On the east coast, Robert Moog was creating synthesizers that attempted to mimic the sounds of accoustic instruments. Meanwhile, on the west-coast, Don Buchla, was creating instruments for creating novel electronic music, amongst the milieu of free-spirited California hippies. Today with the resurgence of modular synths while people still make the conceptual distinction no-one will really adhere to one school or the other.
The basic modular functions are: signal, control, logic/timing.


## Signals

The examples are files that will run on [VCV Rack](https://vcvrack.com/), a virtual modular synthesizer.
Signals are they things that we want to create and manipulate. So usually, this will be audio.

### VCO

VCOs (Voltage Controlled Oscilators) are generally the fundamental building block of any sound that a synth will create.
The 4 fundamental wave types you will find are sine, triangle, saw and square.
Every sound that we hear can be shown to be composed of a combination of sine waves (please don't ask for evidence of that ;) ). On their own they can sound dull and uninteresting.
Triangle waves are similar to sine waves in their sound but have a bit more buzz due to extra harmnoic content. Harmonic content is what make s up the "timbre" of a sound. We can hear the same
not from a scale, but the harmonic content, i.e. the sines waves that make up the sound, can be completely different - what remains the same is the "fundamental frequency". So when a piano and a guitar both play a C, the reason we hear the same note, is because both sounds will contain (or our brains will hear) a sine wave at the frequency of C.
A saw tooth then contains every overtone of the fundametal frequency, with diminishing strength as the frequency inscreaes. That is, the fundamental frequency, the fundamental doubled in frequency, the fundamental tripled and so forth...
A square wave is similar but it contains only odd harmonics.
The harmonic content of these waves is in reality an ideal, and real-life signals, especially analog ones, will often be far from that.

[example VCV Rack file](examples/signals/vco.vcv)

### VCA

VCAs (Voltage Controlled Amplifier) are essentially automated volume controls (if you've never messed around turning a volume knob up and down with music, you've never lived).
Tones on their own are all well-and-good but it's when we start adding dynamic rhythm that things get exciting.

[example VCV Rack file](examples/signals/vca.vcv)

### VCF

VCFs (Voltage Controlled Frequency) are used to subtract frequencies from signals. The most common type if refered to as a low pass filter. This will let low frequencies thru prevent high frequencies. Remember the overtones in the sawtooth? We can take them away and actually make the saw tooth more like a sine wave, if we want. We can also add resonance, which adds a "wah-wah" type sound when the cut-off frequency is altered. They also come in different flavour such as high pass and band pass and others.

[example VCV Rack file](examples/signals/vcf.vcv)

## Control

Control signals (as opposed to erm, signal signals - I didn't make it up) are the signals that are used to automate control of modules.

### LFO

LFOs (Low Frequency Oscilators) are just like oscilators but they differ in that the waves they output will be low frequency, this will usually be below the audible spectrum.
They can be used to e.g.  control the pitch of an oscilator, the amplitude of a VCA or the cut off freqency of a VCF.

[example VCV Rack file](examples/control/lfo.vcv)

### ADSR

ADSRs (ATTACK DECAY SUSTAIN RELEASE) are the most common type fo "envelope generator". They are commonly used to control the amplitude of VCA, so that we can hear a pluck sound for instance, or a slow attack for a sound more reminiscent of an orchestra string section. They can also be used to control the cut-off of a VCF

[example VCV Rack file](examples/control/adsr.vcv)

### QUANTISER

Quantisers are used to control the pitch of oscilators so that they adhere to musical scales. The most common standard is 1v/octave, so that if the control voltage goes up 1 volt the pitch of the oscilator goes up and octave (doubles in pitch). The pitch will then go up the "chromatic" scale every 12th of a volt. As modulars are often used in expiremental music, you can find quantisers that  use different forms of scales and tuning - just intonation, alpha scale, gamelan etc

[example VCV Rack file](examples/control/quantiser.vcv)

## Trigger, Gates, Clocks, Logic

In order to e.g. trigger an ADSR, we need to use gate or trigger signals. These are basically on/off signals, and are essentially the same thing as binary logic signals that are used in computers.
Gates are signals that contain some information about how long something should happen. For example, if you hold down a key on a keyboard, the gate will remain high for as long as you keeping holding down the key. On the other hand, triggers will be a brief pules, that say "do this thing now". Additionally there are clock signals, which can be repeating gates or triggers, that repeat at a given musical tempo.

### Sequencer

Sequencers can be used to create a sequence of tones or rhythms, so as to create a musical pattern.

[example VCV Rack file](examples/logic/sequencer1.vcv)

[example VCV Rack file](examples/logic/sequencer2.vcv)


## Going further

There's lots of great content about modular synths on the web.

- [Suzanna Cianni giving walk thru of Buchla synth usage](https://www.youtube.com/watch?v=CFD72PXOmxA)
- [Communicating with whales with a synth on a Greenpeace anti-whaling vessel](https://www.youtube.com/watch?v=ficpddKrhi0)
- [tutorial on complex sequencer usage on a Serge synthesizer](https://www.youtube.com/watch?v=_0yQAUkRE-M)
- The Black Mirror: Bandersnatch interactive film has a lot music that would have been made with modulars, e.g. Tangerine Dream



